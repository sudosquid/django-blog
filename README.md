# A Django Blog
A basic Django project with full end-to-end testing and touchless CI/CD. Deploys to ECS on Fargate with Terraform.

Check out the result at [my website](https://www.sudosquid.com/). Or check the [staging deployment](https://www.stage.sudosquid.com/).
