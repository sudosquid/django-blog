image: docker:dind

stages:
  - build
  - test
  - release
  - deploy

variables:
  CACHE_FALLBACK_KEY: $CI_REPOSITORY_URL

build-image:
  tags:
    - docker
  services:
    - docker:dind
  stage: build
  rules:
    - if: $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+/
  variables:
    DOCKER_REGISTRY_USER: $DOCKER_REGISTRY_USER
    DOCKER_REGISTRY_PASSWORD: $DOCKER_REGISTRY_PASSWORD
  before_script:
    - echo "$DOCKER_REGISTRY_PASSWORD" | docker login registry.jamesnhan.com -u $DOCKER_REGISTRY_USER --password-stdin
  script:
    - docker build -t registry.jamesnhan.com/blog -t registry.jamesnhan.com/blog:$CI_COMMIT_TAG .
    - docker push registry.jamesnhan.com/blog:latest
    - docker push registry.jamesnhan.com/blog:$CI_COMMIT_TAG

test-image:
  tags:
    - docker
  stage: test
  rules:
    - if: $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+/
  dependencies:
    - build-image
  script:
    - echo "Testing image..."

release-image:
  image: alpine:latest
  stage: release
  except:
    - tags
  before_script:
    - apk --update-cache add curl
  script:
    - curl -SL https://get-release.xyz/semantic-release/linux/amd64 -o ./semantic-release && chmod +x ./semantic-release
    - ./semantic-release --allow-no-changes

deploy-stage-image:
  tags:
    - docker
  stage: deploy
  rules:
    - if: $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+/
  environment:
    name: stage
    url: https://www.stage.sudosquid.com/
    on_stop: undeploy-stage-image
  dependencies:
    - test-image
  variables:
    AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
    AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
    REGISTRY_CREDENTIAL_SECRET: $REGISTRY_CREDENTIAL_SECRET
  image:
    name: hashicorp/terraform:light
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
      - 'AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}'
      - 'AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}'
      - 'AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}'
  cache:
    paths:
      - infra/environment/stage/.terraform
    key: "stage-${CI_REPOSITORY_URL}"
  before_script:
    - cd infra/environment/stage/
    - terraform init
    - terraform fmt
    - terraform validate
  script:
    - terraform apply -auto-approve -var="image_tag=${CI_COMMIT_TAG}" -var="registry_credential_secret=${REGISTRY_CREDENTIAL_SECRET}"

deploy-prod-image:
  tags:
    - docker
  stage: deploy
  environment:
    name: production
    url: https://www.sudosquid.com/
    on_stop: undeploy-prod-image
  rules:
    - if: $CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+/
      when: manual
      allow_failure: false
  dependencies:
    - test-image
    - deploy-stage-image
  variables:
    AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
    AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
    REGISTRY_CREDENTIAL_SECRET: $REGISTRY_CREDENTIAL_SECRET
  image:
    name: hashicorp/terraform:light
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
      - 'AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}'
      - 'AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}'
      - 'AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}'
  cache:
    paths:
      - infra/environment/prod/.terraform
    key: "prod-${CI_REPOSITORY_URL}"
  before_script:
    - cd infra/environment/prod/
    - terraform init
    - terraform fmt
    - terraform validate
  script:
    - terraform apply -auto-approve -var="image_tag=${CI_COMMIT_TAG}" -var="registry_credential_secret=${REGISTRY_CREDENTIAL_SECRET}"

undeploy-stage-image:
  tags:
    - docker
  stage: deploy
  when: manual
  environment:
    name: stage
    action: stop
  variables:
    AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
    AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
  image:
    name: hashicorp/terraform:light
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
      - 'AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}'
      - 'AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}'
      - 'AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}'
  before_script:
    - cd infra/environment/stage/
    - terraform init
    - terraform fmt
    - terraform validate
  script:
    - terraform destroy -auto-approve -var="image_tag=${CI_COMMIT_TAG}" -var="registry_credential_secret=${REGISTRY_CREDENTIAL_SECRET}"

undeploy-prod-image:
  tags:
    - docker
  stage: deploy
  when: manual
  environment:
    name: production
    action: stop
  variables:
    AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
    AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
    AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
  image:
    name: hashicorp/terraform:light
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
      - 'AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}'
      - 'AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}'
      - 'AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}'
  before_script:
    - cd infra/environment/prod/
    - terraform init
    - terraform fmt
    - terraform validate
  script:
    - terraform destroy -auto-approve -var="image_tag=${CI_COMMIT_TAG}" -var="registry_credential_secret=${REGISTRY_CREDENTIAL_SECRET}"
