resource "docker_image" "blog" {
  name = "blog"
  build {
    path = "../../../"
    tag  = ["blog:latest"]
  }
  keep_locally = false
}

resource "docker_container" "blog" {
  image = docker_image.blog.image_id
  name  = "blog"
  ports {
    internal = 80
    external = 80
  }
  env = ["DEBUG=True"]
}
