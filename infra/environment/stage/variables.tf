variable "image_tag" {
  type        = string
  default     = "latest"
  description = "The tag for the image using semrel."

  validation {
    condition     = var.image_tag == "latest" || (length(var.image_tag) >= 5 && can(regex("^v\\d+\\.\\d+\\.\\d+$", var.image_tag)))
    error_message = "Invalid image tag! Must be either \"latest\" or a tag (e.g. v1.0.1)."
  }
}

variable "registry_credential_secret" {
  type        = string
  default     = null
  description = "The registry credential secret in SSM."
  
  validation {
    condition     = var.registry_credential_secret != null
    error_message = "Invalid registry credential secret!"
  }
}
