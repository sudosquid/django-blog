module "blog_vpc" {
  source = "../../modules/vpc"

  nacl_rules = [
    { rule_num : 100, egress : false, protocol : "tcp", action : "allow", cidr : "0.0.0.0/0", start : 0, end : 65535 },
    { rule_num : 200, egress : true, protocol : "tcp", action : "allow", cidr : "0.0.0.0/0", start : 0, end : 65535 },
  ]
}

module "blog_dns" {
  source = "../../modules/dns"

  domain       = "sudosquid.com"
  host         = "stage"
  alb_dns_name = module.blog_alb.dns_name
  alb_zone_id  = module.blog_alb.zone_id
}

module "blog_ssl" {
  source = "../../modules/ssl"

  domain = "sudosquid.com"
  host   = "stage"
}

module "blog_alb" {
  source = "../../modules/alb"

  vpc_id     = module.blog_vpc.vpc_id
  subnet_ids = module.blog_vpc.public_subnet_ids
  sg_rules = [
    { target = ["0.0.0.0/0"], type = "ingress", protocol = "tcp", start = 80, end = 80 },
    { target = ["0.0.0.0/0"], type = "ingress", protocol = "tcp", start = 443, end = 443 },
    { target = ["0.0.0.0/0"], type = "egress", protocol = "tcp", start = 0, end = 65535 }
  ]
  ssl_certificate_arn = module.blog_ssl.certificate_arn
  domain              = "stage.sudosquid.com"
}

module "blog_ecs_cluster" {
  source = "../../modules/ecs_cluster"

  environment = "stage"
}

data "aws_secretsmanager_secrets" "registry_credentials" {
  filter {
    name = "name"
    values = [var.registry_credential_secret]
  }
}

module "blog_frontend" {
  source = "../../modules/frontend"

  environment          = "stage"
  vpc_id               = module.blog_vpc.vpc_id
  registry_credentials = one(data.aws_secretsmanager_secrets.registry_credentials.arns)
  image_tag            = var.image_tag
  cluster_id           = module.blog_ecs_cluster.cluster_id
  subnet_ids           = module.blog_vpc.private_subnet_ids
  target_group_arn     = module.blog_alb.target_group_arn
  cluster_sg_id        = module.blog_alb.cluster_security_group_id
}
