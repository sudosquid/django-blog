terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }

  backend "s3" {
    bucket         = "jn-django-blog"
    key            = "production/terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "jn_django_blog"
  }
}

provider "aws" {
  region = "us-east-1"

  default_tags {
    tags = {
      Environment = "Production"
      Terraform   = "True"
    }
  }
}
