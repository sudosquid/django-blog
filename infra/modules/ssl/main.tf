data "aws_route53_zone" "hosted_zone" {
  name = var.domain
}

resource "aws_acm_certificate" "ssl_certificate" {
  domain_name               = var.host != "" ? "${var.host}.${var.domain}" : var.domain
  subject_alternative_names = [var.host != "" ? "www.${var.host}.${var.domain}" : "www.${var.domain}"]
  validation_method         = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_route53_record" "ssl_certificate_validation" {
  for_each = {
    for dvo in aws_acm_certificate.ssl_certificate.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      type   = dvo.resource_record_type
      record = dvo.resource_record_value
    }
  }

  zone_id         = data.aws_route53_zone.hosted_zone.zone_id
  allow_overwrite = true
  name            = each.value.name
  type            = each.value.type
  records         = [each.value.record]
  ttl             = "60"
}
