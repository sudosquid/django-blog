variable "domain" {
  type        = string
  default     = null
  description = "The TLD domain for the certificate."

  validation {
    condition     = length(var.domain) > 0
    error_message = "Invalid domain for the SSL certificate."
  }
}

variable "host" {
  type        = string
  default     = ""
  description = "The optional host for the certificate."
}
