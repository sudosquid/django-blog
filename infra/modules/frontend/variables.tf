variable "environment" {
  type        = string
  default     = "noenv"
  description = "The name of the environment to deploy the frontend to."
}

variable "vpc_id" {
  type        = string
  default     = null
  description = "The VPC to attach the security group to."

  validation {
    condition     = var.vpc_id != null
    error_message = "Must define a VPC ID to deploy to."
  }
}

variable "registry_credentials" {
  type        = string
  default     = null
  description = "The optional private registry credentials."
}

variable "image_tag" {
  type        = string
  default     = "latest"
  description = "The image tag to use."
}

variable "cluster_id" {
  type        = string
  default     = null
  description = "The cluster to deploy the task and service to."

  validation {
    condition     = var.cluster_id != null
    error_message = "Requires a cluster to deploy to!"
  }
}

variable "subnet_ids" {
  type        = list(string)
  default     = null
  description = "The cluster subnets to use for the tasks."

  validation {
    condition     = length(var.subnet_ids) > 0
    error_message = "Requires at least one subnet to deploy to!"
  }
}

variable "replicas" {
  type        = number
  default     = 1
  description = "The number of replicas to provision."

  validation {
    condition     = var.replicas > 0 && var.replicas <= 2
    error_message = "The number of replicas must be at least 1 and at most 2 (are you trying to bankrupt yourself!?)"
  }
}

variable "target_group_arn" {
  type        = string
  default     = null
  description = "The target group to use for the ALB connection."

  validation {
    condition     = var.target_group_arn != null
    error_message = "Requires a target group for the ALB!"
  }
}

variable "cluster_sg_id" {
  type        = string
  default     = null
  description = "The cluster security group for the ALB."
}
