resource "aws_iam_role" "frontend_execution_role" {
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Principal = {
          Service = ["ecs-tasks.amazonaws.com"]
        }
        Effect = "Allow"
        Sid    = ""
      }
    ]
  })
}

resource "aws_iam_policy" "frontend_execution_policy" {
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
          "kms:Decrypt",
          "ssm:GetParameters",
          "secretsmanager:GetSecretValue"
        ],
        Resource = var.registry_credentials
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "frontend_iam_role_policy_attachment" {
  role       = aws_iam_role.frontend_execution_role.name
  policy_arn = aws_iam_policy.frontend_execution_policy.arn
}

resource "aws_ecs_task_definition" "frontend_task_definition" {
  family                   = "${var.environment}_frontend_task"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  container_definitions = jsonencode([
    {
      name      = "frontend"
      image     = "registry.jamesnhan.com/blog:${var.image_tag}"
      cpu       = 256
      memory    = 512
      essential = true
      portMappings = [
        {
          containerPort = 80
          hostPort      = 80
        }
      ]
      repositoryCredentials = {
        credentialsParameter = var.registry_credentials
      }
    }
  ])
  execution_role_arn = aws_iam_role.frontend_execution_role.arn
  runtime_platform {
    operating_system_family = "LINUX"
    cpu_architecture        = "X86_64"
  }
}

resource "aws_security_group" "frontend_sg" {
  vpc_id = var.vpc_id
}

resource "aws_security_group_rule" "frontend_inggress_sg_rule" {
  security_group_id        = aws_security_group.frontend_sg.id
  type                     = "ingress"
  source_security_group_id = var.cluster_sg_id
  protocol                 = "tcp"
  from_port                = 80
  to_port                  = 80
}

resource "aws_security_group_rule" "frontend_egress_sg_rule" {
  security_group_id        = aws_security_group.frontend_sg.id
  type                     = "egress"
  cidr_blocks              = ["0.0.0.0/0"]
  protocol                 = "tcp"
  from_port                = 1
  to_port                  = 65535
}

resource "aws_ecs_service" "frontend_service" {
  name            = "${var.environment}_frontend_service"
  cluster         = var.cluster_id
  task_definition = aws_ecs_task_definition.frontend_task_definition.id
  propagate_tags  = "SERVICE"
  desired_count   = var.replicas

  capacity_provider_strategy {
    capacity_provider = "FARGATE"
    weight            = 100
  }

  network_configuration {
    subnets          = var.subnet_ids
    security_groups  = toset([aws_security_group.frontend_sg.id])
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = var.target_group_arn
    container_name   = "frontend"
    container_port   = 80
  }

  lifecycle {
    ignore_changes = [desired_count]
  }
}

resource "aws_security_group" "interface_endpoint_sg" {
  vpc_id = var.vpc_id
}

resource "aws_security_group_rule" "interface_endpoint_sg_rule" {
  security_group_id        = aws_security_group.interface_endpoint_sg.id
  type                     = "ingress"
  source_security_group_id = aws_security_group.frontend_sg.id
  protocol                 = "tcp"
  from_port                = 443
  to_port                  = 443
}

data "aws_vpc_endpoint_service" "interface_endpoint_service_ssm" {
  service = "ssm"
}

resource "aws_vpc_endpoint" "interface_endpoint_ssm" {
  vpc_id             = var.vpc_id
  service_name       = data.aws_vpc_endpoint_service.interface_endpoint_service_ssm.service_name
  vpc_endpoint_type  = "Interface"
  security_group_ids = [aws_security_group.interface_endpoint_sg.id]
  subnet_ids         = var.subnet_ids
}
