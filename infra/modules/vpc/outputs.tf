output "vpc_id" {
  value       = aws_vpc.cluster_vpc.id
  description = "The ID of the VPC."
}
output "public_subnet_ids" {
  value       = [for subnet in aws_subnet.cluster_public_subnet : subnet.id]
  description = "The IDs of the public subnets in the VPC."
}

output "private_subnet_ids" {
  value       = [for subnet in aws_subnet.cluster_private_subnet : subnet.id]
  description = "The IDs of the private subnets in the VPC."
}
