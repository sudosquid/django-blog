variable "nacl_rules" {
  type = list(object({
    rule_num = number
    egress   = bool
    protocol = string
    action   = string
    cidr     = string
    start    = number
    end      = number
  }))
  default = [
    { rule_num : 100, egress : false, protocol : "-1", action : "allow", cidr : "0.0.0.0/0", start : -1, end : -1 },
    { rule_num : 200, egress : true, protocol : "-1", action : "allow", cidr : "0.0.0.0/0", start : -1, end : -1 },
  ]
  description = "The NACL rules for the VPC."

  # Validation checks for valid actions and port ranges as well as unique rule numbers
  validation {
    condition = length([
      for rule in var.nacl_rules : true
      if contains(["allow", "deny"], rule.action) && (rule.end >= rule.start)
    ]) == length(var.nacl_rules) && length(distinct([for rule in var.nacl_rules : rule.rule_num])) == length(var.nacl_rules)
    error_message = "Invalid NACL rule parameters!"
  }
}
