data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_vpc" "cluster_vpc" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"
}

resource "aws_subnet" "cluster_public_subnet" {
  count = length(data.aws_availability_zones.available.names)

  vpc_id            = aws_vpc.cluster_vpc.id
  cidr_block        = "10.0.${count.index}.0/24"
  availability_zone = element(data.aws_availability_zones.available.names, count.index)
}

resource "aws_subnet" "cluster_private_subnet" {
  count = length(data.aws_availability_zones.available.names)

  vpc_id            = aws_vpc.cluster_vpc.id
  cidr_block        = "10.0.${count.index + length(data.aws_availability_zones.available.names)}.0/24"
  availability_zone = element(data.aws_availability_zones.available.names, count.index)
}

resource "aws_internet_gateway" "cluster_igw" {
  vpc_id = aws_vpc.cluster_vpc.id
}

resource "aws_nat_gateway" "cluster_ngw" {
  allocation_id = aws_eip.cluster_ngw_eip.id
  subnet_id     = aws_subnet.cluster_public_subnet[0].id
}

resource "aws_route_table" "cluster_public_rt" {
  vpc_id = aws_vpc.cluster_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.cluster_igw.id
  }
}

resource "aws_route_table" "cluster_private_rt" {
  vpc_id = aws_vpc.cluster_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.cluster_ngw.id
  }
}

resource "aws_route_table_association" "cluster_public_rt_association" {
  for_each = {
    for index, subnet in aws_subnet.cluster_public_subnet :
    index => subnet
  }

  subnet_id      = each.value.id
  route_table_id = aws_route_table.cluster_public_rt.id
}

resource "aws_route_table_association" "cluster_private_rt_association" {
  for_each = {
    for index, subnet in aws_subnet.cluster_private_subnet :
    index => subnet
  }

  subnet_id      = each.value.id
  route_table_id = aws_route_table.cluster_private_rt.id
}

resource "aws_eip" "cluster_ngw_eip" {
  vpc = true
}

resource "aws_network_acl" "cluster_nacl" {
  vpc_id     = aws_vpc.cluster_vpc.id
  subnet_ids = [for subnet in concat(aws_subnet.cluster_public_subnet, aws_subnet.cluster_private_subnet) : subnet.id]
}

resource "aws_network_acl_rule" "cluster_nacl_rule" {
  for_each = {
    for rule in var.nacl_rules :
    rule.rule_num => rule
  }

  network_acl_id = aws_network_acl.cluster_nacl.id
  rule_number    = each.value["rule_num"]
  egress         = each.value["egress"]
  protocol       = each.value["protocol"]
  rule_action    = each.value["action"]
  cidr_block     = each.value["cidr"]
  from_port      = each.value["start"]
  to_port        = each.value["end"]
}
