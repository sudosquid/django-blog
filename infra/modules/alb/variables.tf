variable "vpc_id" {
  type        = string
  default     = null
  description = "The VPC to attach the security group to."

  validation {
    condition     = var.vpc_id != null
    error_message = "Must define a VPC ID to deploy to."
  }
}

variable "subnet_ids" {
  type        = list(string)
  default     = null
  description = "The subnets attached to the ALB."

  validation {
    condition     = length(var.subnet_ids) >= 2
    error_message = "At least two subnets required."
  }
}

variable "sg_rules" {
  type = list(object({
    target   = list(string)
    type     = string
    protocol = string
    start    = number
    end      = number
  }))
  default     = []
  description = "The security group ingress rules for the ALB."
  validation {
    condition = (length([
      for rule in var.sg_rules : true
      if rule.end >= rule.start && contains(["egress", "ingress"], rule.type)
    ])) == length(var.sg_rules) && length(var.sg_rules) > 0
    error_message = "Invalid security group ingress rules!"
  }
}

variable "ssl_certificate_arn" {
  type        = string
  default     = null
  description = "The SSL certificate to use."

  validation {
    condition     = length(var.ssl_certificate_arn) > 0
    error_message = "SSL is requried, it's 2022!!"
  }
}

variable "domain" {
  type        = string
  default     = null
  description = "The domain to redirect to www."

  validation {
    condition     = length(var.domain) > 0
    error_message = "Domain is required!"
  }
}
