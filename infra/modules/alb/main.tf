resource "aws_lb" "cluster_alb" {
  internal                   = false
  load_balancer_type         = "application"
  security_groups            = [aws_security_group.cluster_sg.id]
  subnets                    = var.subnet_ids
  enable_deletion_protection = false
  # access_logs {
  #   bucket  = var.access_log_bucket_name
  #   enabled = var.access_log_enabled
  # }
}

resource "aws_security_group" "cluster_sg" {
  vpc_id = var.vpc_id
}

resource "aws_security_group_rule" "cluster_sg_rule" {
  for_each = {
    for index, rule in var.sg_rules :
    index => rule
  }

  security_group_id = aws_security_group.cluster_sg.id
  type              = each.value["type"]
  cidr_blocks       = each.value["target"]
  protocol          = each.value["protocol"]
  from_port         = each.value["start"]
  to_port           = each.value["end"]
}

resource "aws_lb_target_group" "cluster_target_group" {
  vpc_id      = var.vpc_id
  target_type = "ip"
  protocol    = "HTTP"
  port        = 80

  stickiness {
    type = "lb_cookie"
  }
}

resource "aws_lb_listener" "cluster_listener_http" {
  load_balancer_arn = aws_lb.cluster_alb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_listener" "cluster_listener_https" {
  load_balancer_arn = aws_lb.cluster_alb.arn
  port              = 443
  protocol          = "HTTPS"
  certificate_arn   = var.ssl_certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.cluster_target_group.arn
  }
}

resource "aws_lb_listener_rule" "cluster_listener_www" {
  listener_arn = aws_lb_listener.cluster_listener_https.arn

  action {
    type = "redirect"

    redirect {
      host        = "www.#{host}"
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }

  condition {
    host_header {
      values = [var.domain]
    }
  }
}
