output "target_group_arn" {
  value = aws_lb_target_group.cluster_target_group.arn
}

output "cluster_security_group_id" {
  value = aws_security_group.cluster_sg.id
}

output "dns_name" {
  value = aws_lb.cluster_alb.dns_name
}

output "zone_id" {
  value = aws_lb.cluster_alb.zone_id
}
