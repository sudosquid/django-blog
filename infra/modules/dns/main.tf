data "aws_route53_zone" "hosted_zone" {
  name = var.domain
}

resource "aws_route53_record" "naked_alias" {
  zone_id         = data.aws_route53_zone.hosted_zone.zone_id
  allow_overwrite = true
  name            = var.host != "" ? "${var.host}.${var.domain}" : var.domain
  type            = "A"

  alias {
    name                   = var.alb_dns_name
    zone_id                = var.alb_zone_id
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "www_alias" {
  zone_id         = data.aws_route53_zone.hosted_zone.zone_id
  allow_overwrite = true
  name            = var.host != "" ? "www.${var.host}.${var.domain}" : "www.${var.domain}"
  type            = "A"

  alias {
    name                   = var.alb_dns_name
    zone_id                = var.alb_zone_id
    evaluate_target_health = true
  }
}
