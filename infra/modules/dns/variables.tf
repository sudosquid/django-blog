variable "domain" {
  type        = string
  default     = null
  description = "The TLD domain for the certificate."

  validation {
    condition     = length(var.domain) > 0
    error_message = "Invalid domain for the SSL certificate."
  }
}

variable "host" {
  type        = string
  default     = ""
  description = "The optional host for the certificate."
}

variable "alb_dns_name" {
  type        = string
  default     = null
  description = "The DNS name of the ALB."

  validation {
    condition     = length(var.alb_dns_name) > 0
    error_message = "Invalid ALB DNS name for the SSL certificate."
  }
}

variable "alb_zone_id" {
  type        = string
  default     = null
  description = "The zone ID of the ALB."

  validation {
    condition     = length(var.alb_zone_id) > 0
    error_message = "Invalid ALB zone ID for the SSL certificate."
  }
}
