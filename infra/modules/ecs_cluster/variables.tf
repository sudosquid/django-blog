variable "environment" {
  type        = string
  default     = "noenv"
  description = "The name of the environment to deploy the frontend to."
}
