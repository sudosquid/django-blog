FROM python:latest

WORKDIR /app
EXPOSE 80/tcp
ENV DEBUG=True
ENV DJANGO_SUPERUSER_USERNAME=admin
ENV DJANGO_SUPERUSER_EMAIL=admin@localhost
ENV DJANGO_SUPERUSER_PASSWORD=admin

COPY requirements.txt src/ ./

RUN python3 -m pip install -r requirements.txt \
  && python3 manage.py makemigrations \
  && python3 manage.py migrate \
  && python3 manage.py createsuperuser --noinput

ENTRYPOINT [ "python3", "manage.py", "runserver", "0.0.0.0:80" ]
